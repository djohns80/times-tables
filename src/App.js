import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';
import { indigo, red, green, orange } from '@mui/material/colors';

const App = () => {
  const rows = 12;
  const cols = 12;
  const [show, setShow] = React.useState(Array.from({ length: (rows + 1) * (cols + 1) }, i => i = false));

  const handleClick = (e) => {
    const id = parseInt(e.target.id);
    let newArr = [...show];
    if (e.detail === 1) {
      const col = id % (cols + 1);
      const row = parseInt(id / (cols + 1));
      if (row === 0 && col === 0) {
        newArr = Array.from({ length: (rows + 1) * (cols + 1) }, i => i = false);
      } else if (row !== 0 && col !== 0) {
        newArr[(row * (rows + 1)) + col] = !newArr[(row * (rows + 1)) + col];
      } else if (row === 0) {
        const trueCount = show.filter((elem, index) => index % (cols + 1) === col && elem).length;
        for (let i = 0; i < rows + 1; i++) {
          newArr[(i * (rows + 1)) + col] = !(trueCount > (rows/2));
        }
      } else if (col === 0) {
        const trueCount = show.filter((elem, index) => parseInt(index / (cols + 1)) === row && elem).length;
        for (let i = 0; i < cols + 1; i++) {
          newArr[(row * (rows + 1)) + i] = !(trueCount > (cols/2));
        }
      }
    } else if (id === 0 && e.detail === 2 ) {
      newArr = Array.from({ length: (rows + 1) * (cols + 1) }, i => i = true);
      for (let i = 1; i < rows + 1; i++) {
        newArr[(i * (rows + 1))] = false;
      }
      for (let i = 1; i < cols + 1; i++) {
        newArr[i] = false;
      }
      newArr[id] = false;
    }
    setShow(newArr);
  }

  const HtmlTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: 'white',
      color: 'black',
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(12),
      border: '1px solid black'
    },
  }));

  return (
    <div style={{ width: '100%' }}>
      <Box sx={{ display: 'grid', gridTemplateColumns: `repeat(${cols + 1}, 1fr)` }}>
        {[...Array(rows + 1).keys()].map((row) =>
          [...Array(cols + 1).keys()].map((col) =>
            <HtmlTooltip
              key={(row * (rows + 1)) + col}
              title={show[(row * (rows + 1)) + col] ? '' : row * col === 0 ? '' : <Typography variant="h5" color="inherit">{row} x {col} = {row * col}</Typography>}
              arrow
              followCursor
            >
              <Box
                id={(row * (rows + 1)) + col}
                onClick={handleClick}
                sx={{
                  bgcolor: show[(row * (rows + 1)) + col] ? 'white' : row === 0 && col === 0 ? red[500] : row === 0 ? orange[500] : col === 0 ? green[500] : indigo[500],
                  color: 'white',
                  p: 0.5,
                  m: 0.5,
                  borderRadius: 1,
                  textAlign: 'center',
                  fontSize: '2rem',
                  fontWeight: '700',
                  fontFamily: 'arial'
                }}
              >
                {row === 0 && col === 0 ? 'x' : row === 0 ? col : col === 0 ? row : row * col}
              </Box>
            </HtmlTooltip>
          )
        )}
      </Box>
    </div>
  );
}

export default App;
